#!/usr/bin/env python
# coding: utf-8

# # Tokyo Olympics 2020 Analysis
# #By- Aarush Kumar
# #Dated: September 06,2021

# In[1]:


from IPython.display import Image
Image(url='https://cdn.mos.cms.futurecdn.net/6JunFB4xWYSmGsbdWLikZX.jpg')


# In[2]:


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import pandas_profiling as pp
import tensorflow as tf
import plotly.express as px
import plotly.graph_objects as go
sns.color_palette('bright')
sns.set(style='darkgrid',rc = {'figure.figsize':(15,8)})
from plotly.offline import iplot


# In[3]:


#reading data sets
df = pd.read_csv('/home/aarush100616/Downloads/Projects/Tokyo Olympics 2020/Tokyo Medals 2021.csv')


# In[4]:


df


# ## EDA

# In[5]:


df.dtypes


# In[6]:


df.shape


# In[7]:


df.size


# In[8]:


df.isnull().sum()


# In[9]:


df.describe()


# In[10]:


df.info()


# In[11]:


df.isnull().sum().sort_values(ascending=False)


# In[12]:


#visuvalising the  null values
plt.figure(figsize=(10,8))
sns.heatmap(df.isnull())
plt.title('Heatmap for checking Null Values')
plt.show()


# In[13]:


df.columns


# In[14]:


df.sort_values(by = 'Rank By Total',inplace = True)


# In[15]:


df.head()


# ## Plots

# In[16]:


fig = px.scatter(data_frame=df,y= 'Total',x='Country',size = 'Total',template="plotly_dark")
fig.update_xaxes(title = "Countries ",showgrid = False)
fig.update_yaxes(title = "Total Number of Medals",showgrid = False)
fig.update_layout(title = "Total Number of Medals won by each country",)
fig.update_traces(hovertemplate = 'Country: %{x}'+'<br> Total medals: %{y}')
fig.show()


# In[17]:


#making list for count of medals and name of medals
medals = []
medals.append(sum(list(df['Gold Medal'].values)))
medals.append(sum(list(df['Silver Medal'].values)))
medals.append(sum(list(df['Bronze Medal'].values)))
Names = ['Gold Medal','Silver Medal','Bronze Medal']


# In[18]:


fig = px.bar(x=Names,y=medals,template='plotly_dark',text=medals )
fig.update_xaxes(title = "Differnt type of medals",showgrid = False)
fig.update_yaxes(title = "Number of medals",showgrid = False)
fig.update_traces(marker_color = ["#ccad00","#c0c0c0","#665D1E"],
                  hovertemplate = 'Medal: %{x}' +'<br>Number of medals: %{y}'  )
fig.update_layout(title = "Medals Count",
                 font=dict(color='#8a8d93'))
fig.show()


# In[19]:


fig = px.pie(names=Names,values=medals,color=Names,
             color_discrete_map={ "Gold Medal":"#ccad00","Silver Medal":"#c0c0c0",
                                                                "Bronze Medal":"#665D1E"})
fig.update_traces(hovertemplate = 'Medal: %{label}' +'<br>Number of medals: %{value}')
fig.update_layout(title = "Medals Count")
fig.show()


# In[20]:


#top 10 countries
fig = px.bar(df, y=df['Country'][:10],
             x=df['Total'][:10],
             color=df['Country'][:10],
            title='Top 10 countries',
            template='ggplot2',
            text = df['Total'][:10])
fig.update_xaxes(title = "Country Name",showgrid = False)
fig.update_yaxes(title="Number of Medals")
fig.update_traces(hovertemplate='Country :%{x}'+'<br> Number of Medals : %{y}' )
fig.show()


# In[21]:


fig = px.bar(df[:10], x="Country", y=["Gold Medal","Silver Medal","Bronze Medal"],
            color_discrete_map={"Gold Medal":"#ccad00",
                               "Silver Medal":"#c0c0c0",
                               "Bronze Medal":"#665D1E"})
fig.update_traces(hovertemplate = 'Medal :%{y}'+'<br> Number of Medals : %{value}'+'<br>country :%{x}')
fig.update_layout(template = "simple_white",title = "Top 10 countries (Differnt medals wise)")
fig.update_xaxes(title = "country")
fig.update_yaxes(title = "Total Medals",showgrid = False)
fig.show()


# In[22]:


#top 10 countries of Gold Medals
df_gold = df.sort_values(by='Gold Medal',ascending=False)
fig = px.bar(df_gold, x=df_gold['Country'][:10],
             y=df_gold['Gold Medal'][:10],
             color=df_gold['Country'][:10],
            title='Top 10 countries with Most Gold Medals',
            template='ggplot2')
fig.update_xaxes(title = "Country Name")
fig.update_yaxes(title="Number of Medals")
fig.update_traces(hovertemplate='Country :%{x}'+'<br> Gold Medals : %{y}' )
fig.show()


# In[23]:


#top 10 countries of Silver Medals
df_silver = df.sort_values(by='Silver Medal',ascending=False)
fig = px.bar(df, y=df_silver['Country'][:10],
             x=df_silver['Silver Medal'][:10],
             color=df_silver['Country'][:10],
            title='Top 10 countries with Most Silver Medals',
            template='simple_white')
fig.update_xaxes(title = "Country Name")
fig.update_yaxes(title="Number of Medals")
fig.update_traces(hovertemplate='Country :%{x}'+'<br> silver Medals : %{y}' )
fig.show()


# In[24]:


#top 10 countries of Bronze Medals
df_bronze = df.sort_values(by='Bronze Medal',ascending=False)
fig = px.bar(df, x=df_bronze['Country'][:10],
             y=df_bronze['Bronze Medal'][:10],
             color=df_bronze['Country'][:10],
            title='Top 10 countries with Most Bronze Medals')
fig.update_xaxes(title = "Country Name")
fig.update_yaxes(title="Number of Medals",showgrid = False)
fig.update_traces(hovertemplate='Country :%{x}'+'<br> Bronze Medals : %{y}' )
fig.update_layout(plot_bgcolor = "#b9bdba",paper_bgcolor = "#b9bdba",font=dict(color='black'))
fig.show()


# ## calculating percentage

# In[25]:


df['Gold %'] = (df['Gold Medal']/df['Total'])*100
df['Silver %'] = (df['Silver Medal']/df['Total'])*100
df['Bronze %'] = (df['Bronze Medal']/df['Total'])*100


# In[26]:


df.head()


# In[27]:


#top 10 countries of Gold Medals
fig = px.bar(df, x=df['Country'][:10],
             y=df['Gold %'][:10],
             color=df['Country'][:10],
            title='Top 10 countries  Gold % ',
            template='ggplot2')
fig.update_xaxes(title = "Country Name",showgrid = False)
fig.update_yaxes(title="percentage of Medals",showgrid = False)
fig.update_traces(hovertemplate='Country :%{x}'+'<br> Gold Medals% : %{y}' )
fig.show()


# In[28]:


fig = px.bar(df, x=df['Country'][:10],
             y=df['Silver %'][:10],
             color=df['Country'][:10],
            title='Top 10 countries  Silver % ',
            )
fig.update_xaxes(title = "Country Name",showgrid = False)
fig.update_yaxes(title="percentage of Medals",showgrid = False)
fig.update_traces(hovertemplate='Country :%{x}'+'<br> Silver Medals% : %{y}' )
fig.update_layout(plot_bgcolor = "#f5faf6",paper_bgcolor = "#f5faf6",font=dict(color='black'))
fig.show()


# In[29]:


fig = px.bar(df, x=df['Country'][:10],
             y=df['Bronze %'][:10],
             color=df['Country'][:10],
            title='Top 10 countries  Bronze % ',
            template='simple_white')
fig.update_xaxes(title = "Country Name",showgrid = False)
fig.update_yaxes(title="percentage of Medals",showgrid = False)
fig.update_traces(hovertemplate='Country :%{x}'+'<br> Bronze Medals% : %{y}' )
fig.show()


# ## Country wise plots

# In[30]:


df = df.set_index("Country")


# In[31]:


df.head()


# In[32]:


class country:
    def __init__(self,country):
        self.country = country
        
    
    def profile(self):
        l = df.loc[self.country].values
        print("Country : {}".format(self.country))
        print("\n")
        print("Total Medals : {}".format(int(l[3])))
        print("\n")
        print("Final Rank : {}".format(int(l[4])))
        
    def pie_chart(self):
        l = df.loc[self.country][:3].values
        fig = px.pie(names=Names,values=l,color=Names
             ,color_discrete_map={ "Gold Medal":"#ccad00","Silver Medal":"#c0c0c0",
                                                                "Bronze Medal":"#665D1E"})
        fig.update_traces(hovertemplate = 'Medal: %{label}' +'<br>Number of medals: %{value}')
        fig.update_layout(title = "{} Medals Count".format(self.country))
        fig.show()
    
    def medals_bar(self):
        l = df.loc[self.country][:3].values
        fig = px.bar(x=Names,y=l,template='plotly_dark',text=l )
        fig.update_xaxes(title = "Differnt type of medals",showgrid = False)
        fig.update_yaxes(title = "Number of medals",showgrid = False)
        fig.update_traces(marker_color = ["#ccad00","#c0c0c0","#665D1E"],
                  hovertemplate = 'Medal: %{x}' +'<br>Number of medals: %{y}'  )
        fig.update_layout(title = "{} Medals Count".format(self.country),
                 font=dict(color='#8a8d93'))
        fig.show()
    
    def medals_per_bar(self):
        l = df.loc[self.country][5:].values
        fig = px.bar(x=Names,y=l,template='ggplot2',text=l )
        fig.update_xaxes(title = "Differnt percentage  of medals",showgrid = False)
        fig.update_yaxes(title = "Number of medals",showgrid = False)
        fig.update_traces(marker_color = ["#ccad00","#c0c0c0","#665D1E"],
                  hovertemplate = 'Medal: %{x}' +'<br>percentage of medals: %{y}'  )
        fig.update_layout(title = "{} Medals percentage".format(self.country),
                 font=dict(color='#8a8d93'))
        fig.show()


# In[33]:


usa = country("United States of America")


# In[35]:


usa.profile()


# In[36]:


usa.medals_bar()


# In[37]:


usa.pie_chart()


# In[38]:


usa.medals_per_bar()


# In[39]:


ind = country("India")


# In[40]:


ind.profile()


# In[41]:


ind.pie_chart()


# In[42]:


ind.medals_bar()


# In[43]:


ind.medals_per_bar()


# In[44]:


roc = country("People's Republic of China")


# In[45]:


roc.profile()


# In[46]:


roc.pie_chart()


# In[47]:


roc.medals_bar()


# In[48]:


ind.medals_per_bar()

